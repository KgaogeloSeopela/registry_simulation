using Microsoft.Data.Sqlite;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Windows.Forms;

namespace TestDBLite
{
    public partial class Form1 : Form
    {
        private SqliteConnection con;
        private List<int> product_Ids = new List<int>();
        private Dictionary<int, string> products = new Dictionary<int, string>();
        private RandomNumberGenerator randomNo;
        private CardNumbers cardNo;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            var databaseCon = new DatabaseConnection();
            con = databaseCon.ConnectToDB();
            AddItemsToListBox();
            CheckBtnEnabled();
            randomNo = new RandomNumberGenerator();
            cardNo = new CardNumbers();
            txtCardNumber.Visible = false;
            rdbConfirmPay.Visible = false;
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            CheckBtnEnabled();
            rdbConfirmPay.Visible = true;
            txtCardNumber.Visible = false;
        }

        private SqliteDataReader MakeSQLCalls(string query)
        {
            con.Open();
            var cmd = new SqliteCommand(query, con);
            var rdr = cmd.ExecuteReader();

            return rdr;
        }

        private void AddItemsToListBox()
        {
            var rdr = MakeSQLCalls("Select * from Products");

            var count = 0;

            while (rdr.Read())
            {
                clbListView.Items.Insert(count, $"{rdr.GetString(1)} R{rdr.GetInt32(3)}");
                products.Add(rdr.GetInt32(0), rdr.GetString(1));
                count++;
            }

            con.Close();
        }

        private void btnProcessPayment_Click(object sender, EventArgs e)
        {
            string formattedCardNo = string.Empty;
            string ids = string.Empty;

            if (txtCardNumber.Visible == true)
            {
                var isValid = cardNo.validateCardNumber(txtCardNumber.Text);
                if (!isValid)
                {
                    MessageBox.Show("Invalid card number entered. Please ensure that you card number is 16 digits and consists of numerical values", "Error");
                    return;
                }
                formattedCardNo = cardNo.MaskCardNumber(txtCardNumber.Text);
            }

            foreach (var id in product_Ids)
            {
                if (!String.IsNullOrEmpty(ids))
                    ids = ids + "," + id.ToString();
                else
                    ids = id.ToString();
            }

            string sqlCommand = $"select * from products p inner join store_details s on p.store_id = s.store_id inner join supplier sp on p.supplier_id = sp.supplier_id where p.prod_Id in ({ids})";

            string receiptNumber = randomNo.RandomVarchar();

            var data = getJsonData(sqlCommand, formattedCardNo, receiptNumber);

            //dynamic dynamicObject = JsonConvert.SerializeObject(data);
            string dynamicObject = data;


            try
            {
                //Making the HTTP call
                MakeHTTPCall(data);
            }
            catch (HttpRequestException ex)
            {
                throw;
            }
            finally
            {
                btnProcessPayment.Enabled = false;
                rdbCard.Checked = false;
                rdbCash.Checked = false;
                rdbConfirmPay.Checked = false;
                txtCardNumber.Text = null;
                txtCardNumber.Visible = false;
                rdbConfirmPay.Visible = false;
                lblTotal.Text = string.Empty;

                while (clbListView.CheckedIndices.Count > 0)
                {
                    clbListView.SetItemChecked(clbListView.CheckedIndices[0], false);
                }
            }

            con.Close();
        }

        private string getJsonData(string query, string cardNumber, string receiptNumber)
        {
            var rdr = MakeSQLCalls(query);
            var res = new List<Object>();

            while (rdr.Read())
            {
                var result = new
                {
                    Prod_Id = rdr.GetInt32(0).ToString(),
                    Prod_Name = rdr.GetString(1),
                    Prod_Desc = rdr.GetString(2),
                    Price = rdr.GetDecimal(3).ToString(),
                    Store_Id = rdr.GetInt32(4).ToString(),
                    Supplier_Id = rdr.GetInt32(5).ToString(),
                    Store_Name = rdr.GetString(7),
                    Store_Contact = rdr.GetString(8),
                    Street_Name = rdr.GetString(9),
                    City = rdr.GetString(10),
                    Province = rdr.GetString(11),
                    Zip_Code = rdr.GetString(12),
                    Supplier_Name = rdr.GetString(14),
                    Supplier_Contact = rdr.GetString(15),
                    Supplier_Location = rdr.GetString(16),
                    CardNumber = cardNumber,
                    Receipt_Number = receiptNumber,
                    Total_Price = lblTotal.Text,
                    Product_Quantity = 1,
                    Purchase_Date = DateTime.Now.ToString()
                };

                res.Add(result);
            }

            string json = JsonConvert.SerializeObject(new { subArray = res });

            con.Close();

            return json;           
        }

        private void MakeHTTPCall(string json)
        {
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://modest-jepsen-c97719.netlify.app/.netlify/functions/save-receipt");
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(json);
                }

                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                string result;
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    result = streamReader.ReadToEnd();
                }

                MessageBox.Show("Data successfully sent to the data repository", "Success");
            }
            catch(HttpListenerException ex)
            {
                MessageBox.Show("An error occured. Please try again later", "Error");
            }
           
        }

        private void CheckBtnEnabled()
        {
            if (clbListView.CheckedItems.Count == 0)
            {
                btnProcessPayment.Enabled = false;
                return;
            }

            if (!rdbCard.Checked && !rdbCash.Checked)
            {
                btnProcessPayment.Enabled = false;
                return;
            }

            if (rdbConfirmPay.Visible == true && !rdbConfirmPay.Checked)
            {
                btnProcessPayment.Enabled = false;
                return;
            }
            btnProcessPayment.Enabled = true;
        }
        private void clbListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckBtnEnabled();
            decimal price = 0;
            for (int i = 0; i < clbListView.CheckedItems.Count; i++)
            {
                var item = clbListView.CheckedItems[i].ToString().Split();
                var total = (Convert.ToDecimal(item.Last()[1..]));
                price += total;
                var id = products.FirstOrDefault(x => x.Value == item[0]).Key;
                if (!product_Ids.Contains(id))
                    product_Ids.Add(id);
            }

            if (price > 0)
                lblTotal.Text = "R" + price.ToString();
        }

        private void rdbCard_CheckedChanged(object sender, EventArgs e)
        {
            CheckBtnEnabled();
            rdbConfirmPay.Visible = false;
            txtCardNumber.Visible = true;
            rdbConfirmPay.Checked = false;
        }

        private void rdbAccount_CheckedChanged(object sender, EventArgs e)
        {
            CheckBtnEnabled();
            rdbConfirmPay.Visible = false;
            txtCardNumber.Visible = true;
        }

        private void rdbConfirmPay_CheckedChanged(object sender, EventArgs e)
        {
            CheckBtnEnabled();
        }

    }
}
